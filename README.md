## lsg1050-controller

lsg1050-controller is a controller to communicate with a USB CDC Control device.

### functions
* sends and receives data through serial ports, such as a USB serial converter cable.

Technologies: Windows Runtime  
Updates: 04/10/2019
