BUILDTOOLS = C:/Program Files (x86)/Microsoft Visual Studio/2019/BuildTools/

VCINSTALLDIR    = $(BUILDTOOLS)VC/Tools/MSVC/14.23.28105/

WindowsSDKDir   = C:/Program Files (x86)/Windows Kits/10/

VCTools         = $(VCINSTALLDIR)Bin/Hostx64/x64

VCLibraries     = $(VCINSTALLDIR)lib/x64

VCIncludes      = $(VCINSTALLDIR)/include

OSLibrariesUCRT     = $(WindowsSDKDir)Lib/10.0.18362.0/ucrt/x64
OSLibrariesUCRTEnclave     = $(WindowsSDKDir)Lib/10.0.18362.0/ucrt_enclave/x64
OSLibrariesUM     = $(WindowsSDKDir)Lib/10.0.18362.0/um/x64

OSIncludesUCRT      = $(WindowsSDKDir)Include/10.0.18362.0/ucrt
OSIncludesUM      = $(WindowsSDKDir)Include/10.0.18362.0/um
OSIncludesShared      = $(WindowsSDKDir)Include/10.0.18362.0/shared

# Google Test root directory
GTEST_ROOT = ../googletest-release-1.8.1/googletest/
GTEST_INCLUDE = $(GTEST_ROOT)/include/
GTEST_LIB = $(GTEST_ROOT)/msvc/2010/gtest/x64-Debug

GMOCK_ROOT = ../googletest-release-1.8.1/googlemock/
GMOCK_INCLUDE = $(GMOCK_ROOT)/include/
GMOCK_LIB = $(GMOCK_ROOT)/msvc/2010/x64-Debug


SRC_MAIN            = src/main.c
SRC                 = src/controller.c
SRC_TEST            = test/test.cc

BIN                 = bin/
TARGET              = bin/usb-cdc-controller.exe
CC                  = cl.exe


build:  $(SRC_MAIN) $(SRC)
	$(CC) /Fe"$(TARGET)" /Fo"$(BIN)" /Fd"$(BIN)" /ZI /D "WIN32" /D "_DEBUG"  /D "_CONSOLE" /EHsc /MTd /I"$(VCIncludes)" /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)" $(SRC_MAIN) $(SRC) /link /LIBPATH:"$(VCLibraries)" /LIBPATH:"$(OSLibrariesUCRT)" /LIBPATH:"$(OSLibrariesUCRTEnclave)" /LIBPATH:"$(OSLibrariesUM)" /SUBSYSTEM:CONSOLE

test:   $(SRC_TEST) $(SRC)
	$(CC) /Fe"$(TARGET)" /Fo"$(BIN)" /Fd"$(BIN)" /ZI /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /EHsc /MTd  /I"$(VCIncludes)"   /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)" /I"$(GTEST_INCLUDE)" /I"$(GMOCK_INCLUDE)" $(SRC_TEST) /link /LIBPATH:"$(VCLibraries)" /LIBPATH:"$(OSLibrariesUCRT)" /LIBPATH:"$(OSLibrariesUCRTEnclave)" /LIBPATH:"$(OSLibrariesUM)" /LIBPATH:"$(GTEST_LIB)" /LIBPATH:"$(GMOCK_LIB)"

clean:
	rm -rf bin/*

