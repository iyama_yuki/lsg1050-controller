/************************/
/*! @file       controller.c
	@brief      電子負荷(LSG 1050)を制御するコントローラ
	@version    0.1
	@author     Tomoyuki Sugiyama
	@date       2019/09/27
*/
/************************/
/**
 * @mainpage Revision Recode
 *          <td colspan="2">
 *              <table border="3" align="center">
 *              <tr>
 *              <th width="100">Revision</th>   <th width="450">Detail</th> <th width="150">Date</th>   <th width="150">Writer</th>
 *              </tr>
 *              <tr align="center"><th> 1.0 </th>   <td> 新規作成 </td> <td> 2019/10/04 </td>   <td> T.SUGIYAMA </td></tr>
 *              </table>
 *          </td>
 */


 /*!
 * @defgroup        open_usb_group              USB接続を確立(open_usb_lsg_1050)
 * @defgroup        init_group                  初期設定(init_lsg_1050)
 * @defgroup        close_usb_group             USB接続を切断(close_usb_lsg_1050)
 * @defgroup        write_line_group            コマンドを送信(write_line_lsg_1050)
 * @defgroup        read_line_group             データを受信(read_line_lsg_1050)
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#define Print printf
/************************/
/*!
 * @brief           シリアル通信設定をコンソールに出力
 * @param [in]      dcb     Control setting for a serial communications device
*/
/************************/
void PrintCommState(DCB dcb){
	Print("BoudRate = %d, ByteSize = %d. Parity = %d, StopBits = %d EofChar = 0x%x\n",dcb.BaudRate, dcb.ByteSize, dcb.Parity, dcb.StopBits,dcb.EofChar);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)とのUSB接続を確立後、
 *                   シリアル通信設定、受信タイムアウト時間を設定
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  1           CreateFile異常終了<br>
 *                  2           SetupComm異常終了<br>
 *                  3           SetCommState異常終了<br>
 *                  4           SetCommTimeouts異常終了<br>
 *                  5           PurgeComm異常終了<br>
 *                  99          GetCommState異常終了
 * @ingroup         open_usb_group
*/
/************************/
int open_usb_lsg_1050(HANDLE *hCom){
	DCB dcb;
	BOOL fSuccess;
	TCHAR *pcCommPort = TEXT("COM4"); //  LSG1050 is connected to COM4 port
	COMMTIMEOUTS timeouts;
	// Open a handle to the specified com port.
	*hCom = CreateFile( pcCommPort,
						GENERIC_READ | GENERIC_WRITE,
						0,      //  must be opened with exclusive-access
						NULL,   //  default security attributes
						OPEN_EXISTING, //  must use OPEN_EXISTING
						0,      //  not overlapped I/O
						NULL ); //  hTemplate must be NULL for comm devices
	if (*hCom == INVALID_HANDLE_VALUE) 
	{
		// Handle the error.
		Print ("CreateFile failed with error %d.\n", GetLastError());
		return (1);
	}
	// Initialize the DCB structure.
	SecureZeroMemory(&dcb,sizeof(DCB));
	dcb.DCBlength = sizeof(DCB);
	// Set buffer size
	fSuccess = SetupComm(*hCom, 2048, 2048);
	if(!fSuccess){
		Print("SetupComm failed with error %d.\n",GetLastError());
		return (2);
	}
#ifdef DEBUG_LSG_1050
	fSuccess = GetCommState(*hCom,&dcb);
	if(!fSuccess){
		Print("GetCommState failed with error %d.\n",GetLastError());
		return (99);
	}
	Print("Startup communication.");
	PrintCommState(dcb);
#endif
	//  Fill in some DCB values and set the com state: 
	//  9,600 bps, 8 data bits, no parity, parity checking is FALSE,
	//  1 stop bit, and EOF is "LF".
	dcb.BaudRate = CBR_9600;
	dcb.ByteSize = 8;
	dcb.EofChar = 0xa;
	dcb.Parity = NOPARITY;
	dcb.fParity = FALSE;
	dcb.StopBits = ONESTOPBIT;
	fSuccess = SetCommState(*hCom,&dcb);
	if(!fSuccess){
		Print("SetCommState failed with error %d.\n",GetLastError());
		return (3);
	}
	// Set the time-out parameters for a communications device.
	// The maximum time allowed to elapse before the arrival of the next byte
	// on the communications line, in milliseconds.
	timeouts.ReadIntervalTimeout = 50;          // 50ms
	// The multiplier used to calculate the total time-out period for read operations,
	// in milliseconds. 
	timeouts.ReadTotalTimeoutMultiplier = 0;    // 0 ms
	// A constant used to calculate the total time-out period for read operations,
	// in milliseconds.
	timeouts.ReadTotalTimeoutConstant = 100;    // 100 ms
	// The multiplier used to calculate the total time-out period for write operations,
	// in milliseconds. 
	timeouts.WriteTotalTimeoutConstant = 10;    // 10 ms
	// A constant used to calculate the total time-out period for write operations,
	// in milliseconds.
	timeouts.WriteTotalTimeoutMultiplier = 50;  // 50 ms
	fSuccess = SetCommTimeouts(*hCom, &timeouts);
	if(!fSuccess){
		Print("SetCommTimeouts failed with error %d.\n",GetLastError());
		return (4);
	}
#ifdef DEBUG_LSG_1050
	fSuccess = GetCommState(*hCom,&dcb);
	if(!fSuccess){
		Print("GetCommState failed with error %d.\n",GetLastError());
		return (99);
	}
	Print("Set CommState and CommTimeouts.");
	PrintCommState(dcb);
#endif
	// Discards all characters from the output or input buffer 
	// of a specified communications resource.
	fSuccess = PurgeComm(*hCom, PURGE_RXABORT | PURGE_RXCLEAR);
	if(!fSuccess){
		Print("PurgeComm failed with error %d.\n",GetLastError());
		return (5);
	}
	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)に初期設定値を送信
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  0以外     異常終了
 * @ingroup         init_group
*/
/************************/
int init_lsg_1050(HANDLE *hCom){
	int result;
	result = write_line_lsg_1050(hCom,"*cls;:INP OFF;:MODE CC;:INP:MODE LOAD;:MODE:CRAN HIGH;:MODE:VRAN HIGH;:UTIL:LOAD:MODE OFF");
	if(result != 0){
		Print("write_line_lsg_1050 failed.\n");
		return (result);
	}
	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)の出力、リモートをOFFに変更後、
 *                  USB接続を切断
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  0以外     異常終了
 * @ingroup         close_usb_group
*/
/************************/
int close_usb_lsg_1050(HANDLE *hCom){
	int result;
	// Set the state to close: 
	// output OFF and remote OFF
	result = write_line_lsg_1050(hCom,":INP OFF;:UTIL:REM OFF");
	if(result != 0){
		Print("write_line_lsg_1050 failed.\n");
		return (result);
	}
	// ハンドラをクローズ
	CloseHandle (*hCom);
	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)にコマンドを送信
 * @param [in]      hCom        A handle to the communications resource.
 * @param [in]      format      出力コマンド
 * @param [in]      ...         コマンドに設定する引数
 * @return          0           正常終了<br>
 *                  1           PurgeComm異常終了<br>
 *                  2           WriteFile異常終了
 * @ingroup         write_line_group
*/
/************************/
int write_line_lsg_1050(HANDLE *hCom,char *format, ... ){
	va_list va;
	DWORD dwBytesWrite = 10;
	BOOL fSuccess;
	char message[256];
	int messageLenght = 0;
	// Accesses variable-argument lists.
	va_start(va, format);
	// Write formatted output using a pointer to a list of arguments.
	vsprintf((char*)message, format, va);
	va_end(va);
#ifdef DEBUG_LSG_1050
	Print("message == %s",message);
#endif
	messageLenght = strlen(message);
	// EOF is "LF"
	message[messageLenght] = 0xa;
	// Clear send/recv buffer
	fSuccess = PurgeComm(*hCom, PURGE_RXABORT | PURGE_RXCLEAR);
	if(!fSuccess){
		Print("PurgeComm failed with error %d.\n",GetLastError());
		return (1);
	}
	// Send command to LSG1050
	fSuccess = WriteFile(
		*hCom,
		message,
		messageLenght +1,
		&dwBytesWrite,
		NULL
	);
	if(!fSuccess){
		Print("WriteFile failed with error %d.\n",GetLastError());
		return (2);
	}
	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)からデータを受信
 * @param [in]      hCom        A handle to the communications resource.
 * @ingroup         read_line_group
*/
/************************/
void read_line_lsg_1050(HANDLE *hCom){
	BOOL fSuccess;
	PVOID recvMessage[2096];
	DWORD messageLength = 0;
	ReadFile(
		*hCom,
		recvMessage,
		256,
		&messageLength,
		NULL
	);
	Print("%s",(char *)recvMessage);
}
